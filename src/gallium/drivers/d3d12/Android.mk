# Mesa 3-D graphics library
#
# Copyright (C) 2018 Intel Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

LOCAL_PATH := $(call my-dir)

# get C_SOURCES
include $(LOCAL_PATH)/Makefile.sources

include $(CLEAR_VARS)

LOCAL_MODULE := libmesa_pipe_d3d12

LOCAL_SRC_FILES := \
	$(D3D12_C_SOURCES)

LOCAL_C_INCLUDES := \
	$(MESA_TOP)/include \
	$(MESA_TOP)/include/android_stub \
	$(MESA_TOP)/src \
	$(MESA_TOP)/src/mesa \
	$(MESA_TOP)/src/gallium/include \
	$(MESA_TOP)/src/gallium/auxiliary \
	$(MESA_TOP)/src/microsoft/compiler \
	$(MESA_TOP)/src/microsoft/resource_state_manager \
	$(MESA_TOP)/subprojects/DirectX-Headers-1.0/include \
	$(MESA_TOP)/subprojects/DirectX-Headers-1.0/include/wsl/stubs

LOCAL_STATIC_LIBRARIES := \
    libmesa_nir \
	libmesa_util \

LOCAL_WHOLE_STATIC_LIBRARIES := \
	libdxil_compiler \
    libd3d12_resource_state


include $(GALLIUM_COMMON_MK)
include $(BUILD_STATIC_LIBRARY)


ifneq ($(HAVE_GALLIUM_D3D12),)
GALLIUM_TARGET_DRIVERS += d3d12
$(eval GALLIUM_LIBS += $(LOCAL_MODULE))
$(eval GALLIUM_SHARED_LIBS += $(LOCAL_SHARED_LIBRARIES))
endif
